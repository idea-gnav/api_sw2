package com.businet.SW2App.models;

import java.util.List;

public class ConditionModel {

	String conditionCode;
	String conditionName;
	List<TpNoModel> tpNoList;
	
	public String getConditionCode() {
		return conditionCode;
	}
	public void setConditionCode(String conditionCode) {
		this.conditionCode = conditionCode;
	}
	public String getConditionName() {
		return conditionName;
	}
	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}
	public List<TpNoModel> getTpNoList() {
		return tpNoList;
	}
	public void setTpNoList(List<TpNoModel> tpNoList) {
		this.tpNoList = tpNoList;
	}

}
