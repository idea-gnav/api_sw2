package com.businet.SW2App.models;

public class CheckTypeModel {

	int inspectionId;
	String inspectionName;
	
	
	public int getInspectionId() {
		return inspectionId;
	}
	public void setInspectionId(int inspectionId) {
		this.inspectionId = inspectionId;
	}
	public String getInspectionName() {
		return inspectionName;
	}
	public void setInspectionName(String inspectionName) {
		this.inspectionName = inspectionName;
	}
	
}
