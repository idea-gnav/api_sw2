package com.businet.SW2App.models;

import java.sql.Date;

public class CampaignModel {

	String campaignNo;
	String exeReason;
	String issueDate;
	String campaignDueDate;
	String campaignType;
	int fileCount;
	
	public String getCampaignNo() {
		return campaignNo;
	}
	public void setCampaignNo(String campaignNo) {
		this.campaignNo = campaignNo;
	}
	public String getExeReason() {
		return exeReason;
	}
	public void setExeReason(String exeReason) {
		this.exeReason = exeReason;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getCampaignDueDate() {
		return campaignDueDate;
	}
	public void setCampaignDueDate(String campaignDueDate) {
		this.campaignDueDate = campaignDueDate;
	}
	public String getCampaignType() {
		return campaignType;
	}
	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}
	public int getFileCount() {
		return fileCount;
	}
	public void setFileCount(int fileCount) {
		this.fileCount = fileCount;
	}
	
	
	
	

}
