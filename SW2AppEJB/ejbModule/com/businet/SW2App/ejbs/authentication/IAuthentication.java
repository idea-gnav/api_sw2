package com.businet.SW2App.ejbs.authentication;

import com.businet.SW2App.entities.AuthenticationToken;

public interface IAuthentication {
	public AuthenticationToken generateToken(String DeviceId, String loginId);
	
	public boolean verifyToken(String token);
}
