package com.businet.SW2App.models;

public class WorkSiteModel {
	String workSiteCode;
	String workSiteName;
	
	public String getWorkSiteCode() {
		return workSiteCode;
	}
	public void setWorkSiteCode(String workSiteCode) {
		this.workSiteCode = workSiteCode;
	}
	public String getWorkSiteName() {
		return workSiteName;
	}
	public void setWorkSiteName(String workSiteName) {
		this.workSiteName = workSiteName;
	}

}
