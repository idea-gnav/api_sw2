package com.businet.SW2App.models;

import java.util.List;

public class PartsModel {
	String partsCode;
	String partsName;
	List<ConditionModel> conditionList;
	List<AttachmentCrackModel> attachmentCrackList;
	
	public String getPartsCode() {
		return partsCode;
	}
	public void setPartsCode(String partsCode) {
		this.partsCode = partsCode;
	}
	public String getPartsName() {
		return partsName;
	}
	public void setPartsName(String partsName) {
		this.partsName = partsName;
	}
	public List<ConditionModel> getConditionList() {
		return conditionList;
	}
	public void setConditionList(List<ConditionModel> conditionList) {
		this.conditionList = conditionList;
	}
	public List<AttachmentCrackModel> getAttachmentCrackList() {
		return attachmentCrackList;
	}
	public void setAttachmentCrackList(List<AttachmentCrackModel> attachmentCrackList) {
		this.attachmentCrackList = attachmentCrackList;
	}
	
}
