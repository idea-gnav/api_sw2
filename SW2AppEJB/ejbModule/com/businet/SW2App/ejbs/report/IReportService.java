package com.businet.SW2App.ejbs.report;

import java.util.List;

public interface IReportService {

	
	String getProcessDefinCodeById(Integer id);
	
	//�����X�g
	List<Object[]> getCountryListByLanguage(Integer deviceLanguage);
	
	List<Object[]> getCodeValueDataByCodeType(Integer deviceLanguage, String codeType);
	
	List<Object[]> getCheckTypeListByMachineNumber(String machineNumber);
	
	List<Object[]> getCampaignListByMachineNumber(Integer deviceLanguage,String machineNumber);
	
	List<Object[]> getAreaList(Integer deviceLanguage,String models);
	
	Object[] getMachineInfoById(Integer machineId);
	
	List<Object[]> getWarrantyList(Integer deviceLanguage,Integer machineId);
	
	
}
