package com.businet.SW2App.models;

import java.util.List;

public class ReportChoiceListModel extends BaseModel {

	List<CountryModel> countryList;
	List<MachineAttachmentModel> machineAttachmentList;
	List<CheckTypeModel> checkTypeList;
	List<ResultModel> resultList;
	List<ReportTypeModel> reportTypeList;
	List<ReportTypeModel> paidTypeList;
	List<MachineStatusModel> machineStatusList;
	List<CampaignModel> campaignList;
	List<AreaModel> areaList;
	List<WarrantyModel> warrantyList;
	List<AttachmentModel> attachmentList;
	List<WorkSiteModel> workSiteList;
	
	public List<CountryModel> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<CountryModel> countryList) {
		this.countryList = countryList;
	}
	public List<MachineAttachmentModel> getMachineAttachmentList() {
		return machineAttachmentList;
	}
	public void setMachineAttachmentList(List<MachineAttachmentModel> machineAttachmentList) {
		this.machineAttachmentList = machineAttachmentList;
	}
	public List<CheckTypeModel> getCheckTypeList() {
		return checkTypeList;
	}
	public void setCheckTypeList(List<CheckTypeModel> checkTypeList) {
		this.checkTypeList = checkTypeList;
	}
	public List<ResultModel> getResultList() {
		return resultList;
	}
	public void setResultList(List<ResultModel> resultList) {
		this.resultList = resultList;
	}
	public List<ReportTypeModel> getReportTypeList() {
		return reportTypeList;
	}
	public void setReportTypeList(List<ReportTypeModel> reportTypeList) {
		this.reportTypeList = reportTypeList;
	}
	public List<ReportTypeModel> getPaidTypeList() {
		return paidTypeList;
	}
	public void setPaidTypeList(List<ReportTypeModel> paidTypeList) {
		this.paidTypeList = paidTypeList;
	}
	public List<MachineStatusModel> getMachineStatusList() {
		return machineStatusList;
	}
	public void setMachineStatusList(List<MachineStatusModel> machineStatusList) {
		this.machineStatusList = machineStatusList;
	}
	public List<CampaignModel> getCampaignList() {
		return campaignList;
	}
	public void setCampaignList(List<CampaignModel> campaignList) {
		this.campaignList = campaignList;
	}
	public List<AreaModel> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}
	public List<WarrantyModel> getWarrantyList() {
		return warrantyList;
	}
	public void setWarrantyList(List<WarrantyModel> warrantyList) {
		this.warrantyList = warrantyList;
	}
	public List<AttachmentModel> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<AttachmentModel> attachmentList) {
		this.attachmentList = attachmentList;
	}
	public List<WorkSiteModel> getWorkSiteList() {
		return workSiteList;
	}
	public void setWorkSiteList(List<WorkSiteModel> workSiteList) {
		this.workSiteList = workSiteList;
	}

}
