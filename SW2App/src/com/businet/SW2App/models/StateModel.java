package com.businet.SW2App.models;

import java.util.List;

public class StateModel {

	String stateCode;
	String stateName;
	List<CityModel> cityList;
	
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public List<CityModel> getCityList() {
		return cityList;
	}
	public void setCityList(List<CityModel> cityList) {
		this.cityList = cityList;
	}

	
	
}
