package com.businet.SW2App.models;

public class AttachmentCrackModel {
	String crackCode;
	String crackName;
	
	public String getCrackCode() {
		return crackCode;
	}
	public void setCrackCode(String crackCode) {
		this.crackCode = crackCode;
	}
	public String getCrackName() {
		return crackName;
	}
	public void setCrackName(String crackName) {
		this.crackName = crackName;
	}

}
