package com.businet.SW2App;

public class Constants {

	public static final String CON_AUTHORIZED_PUBLIC="Public";
	public static final String CON_AUTHORIZED_REQUIRED="Required";

	public static final int CON_OK = 1000;							// 成功
//	public static final int CON_WARNING_PASSWORD_3DAYS = 1001;		// 認証成功、パスワード有効期限切れ間近(3日）
	public static final int CON_WARNING_PASSWORD_EXPIRED = 1101;	// パスワード有効期限切れ、初回利用時パスワード変更
	public static final int CON_WARNING_NO_USER_PASSWORD = 1201;	// ユーザーIDまたはパスワード誤り
	public static final int CON_WARNING_APP_VERSION = 1301;			// アプリケーションバージョン警告
	public static final int CON_WARNING_MAXIMUN_NUMBER = 1401;		// 検索件数が最大件数を超える
//	public static final int CON_WARNING_PASSWORD_7DAYS = 1501;		// 認証成功、パスワード有効期限切れ間近(7日）
	public static final int CON_WARNING_NO_LATLNG = 1601;			// 特定位置集権検索の機番が存在しない又は機番に緯度経度がない
	public static final int CON_UNKNOWN_ERROR = 1002;				// 例外

	
	public static final String SW2_HOST_URL = "http://52.198.146.209";
	public static final String REPORT_TYPE_010 = "010";
	public static final String REPORT_TYPE_020 = "020";
	public static final String REPORT_TYPE_030 = "030";
	public static final String REPORT_TYPE_040 = "040";
	public static final String REPORT_TYPE_050 = "050";
	public static final String REPORT_TYPE_055 = "055";
	public static final String REPORT_TYPE_060 = "060";
	public static final String REPORT_TYPE_070 = "070";
	public static final String REPORT_TYPE_080 = "080";
	public static final String REPORT_TYPE_090 = "090";
	
	
	
	
	
	
	
	public static final String HOST_URL = "http://192.168.0.108:9080/SW2App/";

	/*
	public static final String APP_LOG_PATH = "C:\\Users\\ShareVC\\Desktop\\Sumitomo\\Server\\Local\\Logs";
	public static final String APP_CONFIG_PATH = "C:\\Users\\ShareVC\\Desktop\\Sumitomo\\Server\\Local\\config_gnav.properties";
				
	public static final String FILE_STORAGE_IMAGE_PATH = "/home/profile01/GnavAppFiles/Images/GNavImagesUpload/";
	public static final String UPLOADED_IMAGE_URL = "http://113.33.116.157:14908/Fileserver/GnavAppFiles/Images/GNavImagesUpload/";
	*/
	
	
}
