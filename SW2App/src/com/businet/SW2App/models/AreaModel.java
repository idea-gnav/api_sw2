package com.businet.SW2App.models;

import java.util.List;

public class AreaModel {

	String areaCode;
	String areaName;
	List<PartsModel> partsList;
	
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public List<PartsModel> getPartsList() {
		return partsList;
	}
	public void setPartsList(List<PartsModel> partsList) {
		this.partsList = partsList;
	}

}
