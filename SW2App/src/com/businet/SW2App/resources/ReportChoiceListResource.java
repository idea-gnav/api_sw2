package com.businet.SW2App.resources;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.businet.SW2App.Constants;
import com.businet.SW2App.annotations.Authorized;
import com.businet.SW2App.ejbs.report.IReportService;
import com.businet.SW2App.http.HttpServices;
import com.businet.SW2App.http.HttpServices.RequestType;
import com.businet.SW2App.models.AreaModel;
import com.businet.SW2App.models.AttachmentCrackModel;
import com.businet.SW2App.models.AttachmentModel;
import com.businet.SW2App.models.CampaignModel;
import com.businet.SW2App.models.CheckTypeModel;
import com.businet.SW2App.models.CityModel;
import com.businet.SW2App.models.ConditionModel;
import com.businet.SW2App.models.CountryModel;
import com.businet.SW2App.models.MachineAttachmentModel;
import com.businet.SW2App.models.MachineStatusModel;
import com.businet.SW2App.models.PartsModel;
import com.businet.SW2App.models.ReportChoiceListModel;
import com.businet.SW2App.models.ReportMenuListModel;
import com.businet.SW2App.models.ReportTypeModel;
import com.businet.SW2App.models.ResultModel;
import com.businet.SW2App.models.ReturnContainer;
import com.businet.SW2App.models.StateModel;
import com.businet.SW2App.models.TpNoModel;
import com.businet.SW2App.models.WarrantyModel;
import com.businet.SW2App.models.WorkSiteModel;

@Path("/reportChoiceList")
@Stateless
public class ReportChoiceListResource {
	private static final Logger logger = Logger.getLogger(ReportChoiceListResource.class.getName());

	@EJB
	IReportService reportService;

	/**
	 * [API No.9] レポート入力選択肢リスト
	 * 
	 * @param sw2UserId
	 * @param reportType
	 * @param machineId
	 * @param deviceLanguage
	 * @return Response.JSON ReportChoiceListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("sw2UserId") String sw2UserId, @FormParam("reportType") String reportType,
			@FormParam("machineId") Integer machineId, @FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] sw2UserId=" + sw2UserId + ", reportType=" + reportType + ",machineId=" + machineId
				+ ", deviceLanguage=" + deviceLanguage);

		try {

			ReportChoiceListModel reportChoiceList = new ReportChoiceListModel();

			LocalDateTime startDate = LocalDateTime.now();
			LocalDateTime startDateRoot = LocalDateTime.now();

			System.out.println("---START at: " + startDate);
			List<Object[]> countryData = reportService.getCountryListByLanguage(deviceLanguage);

			if (countryData != null) {

				List<CountryModel> countryList = new ArrayList<CountryModel>();
				String cnCode = "";
				CountryModel country = null;

				String stCode = "";
				List<StateModel> stateList = null;
				StateModel state = null;

				List<CityModel> cityList = null;

				int size = countryData.size();
				for (int i = 0; i < size; i++) {

					Object[] obj = countryData.get(i);

					if (obj[0] == null)
						break;
					// 国
					String ctryCd = obj[0].toString();

					if (!ctryCd.contentEquals(cnCode)) {

						if (country != null) {
							if (cityList != null) {
								if (state == null)
									state = new StateModel();

								state.setCityList(cityList);
							}

							if (state != null) {
								if (stateList == null)
									stateList = new ArrayList<StateModel>();

								stateList.add(state);
							}

							if (stateList != null)
								country.setStateList(stateList);

							countryList.add(country);
						}

						cnCode = ctryCd;
						country = new CountryModel();
						country.setCountryCode(ctryCd);
						if (obj[1] != null)
							country.setCountryName(obj[1].toString());

						stateList = null;
						cityList = null;
						state = null;
						stCode = "";
					}

					// 州
					if (obj[2] == null)
						break;

					String stCd = obj[2].toString();

					if (!stCd.contentEquals(stCode)) {

						if (stateList == null)
							stateList = new ArrayList<StateModel>();

						if (cityList != null) {
							if (state == null)
								state = new StateModel();

							state.setCityList(cityList);
						}

						if (state != null)
							stateList.add(state);

						state = new StateModel();
						cityList = null;

						stCode = stCd;
						state.setStateCode(stCd);

						if (obj[3] != null)
							state.setStateName(obj[3].toString());
					}

					// 市
					if (obj[4] != null) {
						CityModel city = new CityModel();
						city.setCityCode(obj[4].toString()); // 市コード

						if (obj[5] != null)
							city.setCityName(obj[5].toString()); // 市名

						if (cityList == null)
							cityList = new ArrayList<CityModel>();

						cityList.add(city);
					}
				}

				// 最後データ
				if (country != null) {
					if (cityList != null) {
						if (state == null)
							state = new StateModel();

						state.setCityList(cityList);
					}

					if (state != null) {
						if (stateList == null)
							stateList = new ArrayList<StateModel>();

						stateList.add(state);
					}

					if (stateList != null)
						country.setStateList(stateList);

					countryList.add(country);
				}

				if (countryList.size() > 0)
					reportChoiceList.setCountryList(countryList);

			}

			System.out.println("---END get countryList at: " + LocalDateTime.now() + " | Total: "
					+ Duration.between(startDate, LocalDateTime.now()).toMillis());

			startDate = LocalDateTime.now();

			// 0.機番情報取得
			Object[] kibanInfo = reportService.getMachineInfoById(machineId);
			String machineNumber = "";
			String sumModel1 = "";
			if (kibanInfo != null) {
				if (kibanInfo[1] != null)
					machineNumber = kibanInfo[1].toString();
				if (kibanInfo[2] != null)
					sumModel1 = kibanInfo[2].toString();
			}

			// 機械アタッチメントリスト取得
			List<Object[]> rs = reportService.getCodeValueDataByCodeType(deviceLanguage, "0014");

			if (rs != null && rs.size() > 0) {
				List<MachineAttachmentModel> machineAttachments = new ArrayList<MachineAttachmentModel>();
				for (Object[] obj : rs) {
					MachineAttachmentModel attachmentModel = new MachineAttachmentModel();

					attachmentModel.setMachineAttachmentCode(obj[0].toString());
					if (obj[1] != null)
						attachmentModel.setMachineAttachmentName(obj[1].toString());

					machineAttachments.add(attachmentModel);
				}

				reportChoiceList.setMachineAttachmentList(machineAttachments);
			}

			System.out.println("---END get machine attachment list at: " + LocalDateTime.now() + " | Total: "
					+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			startDate = LocalDateTime.now();

			// 点検種別リスト
			if (machineId != null) {
				List<Object[]> checkTypeData = reportService.getCheckTypeListByMachineNumber(machineNumber);
				if (checkTypeData != null) {
					List<CheckTypeModel> checkTypeList = new ArrayList<CheckTypeModel>();
					for (Object[] obj : checkTypeData) {
						CheckTypeModel checkType = new CheckTypeModel();

						checkType.setInspectionId(Integer.parseInt(obj[0].toString()));
						if (obj[1] != null)
							checkType.setInspectionName(obj[1].toString());

						checkTypeList.add(checkType);
					}

					reportChoiceList.setCheckTypeList(checkTypeList);

					System.out.println("---END get Check Type list at: " + LocalDateTime.now() + " | Total: "
							+ Duration.between(startDate, LocalDateTime.now()).toMillis());
				}
			}
			startDate = LocalDateTime.now();

			// 結果リスト
			List<Object[]> rsList = reportService.getCodeValueDataByCodeType(deviceLanguage, "0034");

			if (rsList != null && rsList.size() > 0) {
				List<ResultModel> ResultList = new ArrayList<ResultModel>();
				for (Object[] obj : rsList) {
					ResultModel result = new ResultModel();

					result.setResultCode(obj[0].toString());
					if (obj[1] != null)
						result.setResultName(obj[1].toString());

					ResultList.add(result);
				}

				reportChoiceList.setResultList(ResultList);
			}

			System.out.println("---END get ResultList list at: " + LocalDateTime.now() + " | Total: "
					+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			startDate = LocalDateTime.now();

			// レポートタイプリスト
			List<Object[]> reportTyoeList = reportService.getCodeValueDataByCodeType(deviceLanguage, "0021");

			if (reportTyoeList != null && reportTyoeList.size() > 0) {
				List<ReportTypeModel> reportList = new ArrayList<ReportTypeModel>();
				for (Object[] obj : reportTyoeList) {
					ReportTypeModel result = new ReportTypeModel();

					result.setReportTypeCode(obj[0].toString());
					if (obj[1] != null)
						result.setReportTypeName(obj[1].toString());

					reportList.add(result);
				}

				reportChoiceList.setReportTypeList(reportList);
			}

			System.out.println("---END get Report Type list at: " + LocalDateTime.now() + " | Total: "
					+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			startDate = LocalDateTime.now();

			// 有償タイプリスト
			List<Object[]> paidTypeList = reportService.getCodeValueDataByCodeType(deviceLanguage, "0036");

			if (paidTypeList != null && paidTypeList.size() > 0) {
				List<ReportTypeModel> reportList = new ArrayList<ReportTypeModel>();
				for (Object[] obj : paidTypeList) {
					ReportTypeModel result = new ReportTypeModel();

					result.setReportTypeCode(obj[0].toString());
					if (obj[1] != null)
						result.setReportTypeName(obj[1].toString());

					reportList.add(result);
				}

				reportChoiceList.setPaidTypeList(reportList);
			}

			System.out.println("---END get paidType list at: " + LocalDateTime.now() + " | Total: "
					+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			startDate = LocalDateTime.now();

			// 不具合時の機械状態リスト
			List<Object[]> machineStatusList = reportService.getCodeValueDataByCodeType(deviceLanguage, "0038");

			if (machineStatusList != null && machineStatusList.size() > 0) {
				List<MachineStatusModel> machineList = new ArrayList<MachineStatusModel>();
				for (Object[] obj : machineStatusList) {
					MachineStatusModel result = new MachineStatusModel();

					result.setMachineStatusCode(obj[0].toString());
					if (obj[1] != null)
						result.setMachineStatusName(obj[1].toString());

					machineList.add(result);
				}

				reportChoiceList.setMachineStatusList(machineList);
			}

			System.out.println("---END get machineStatus list at: " + LocalDateTime.now() + " | Total: "
					+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			startDate = LocalDateTime.now();

			// キャンペーンリスト
			if (machineId != null) {
				List<Object[]> campaignList = reportService.getCampaignListByMachineNumber(deviceLanguage,
						machineNumber);

				if (campaignList != null && campaignList.size() > 0) {
					List<CampaignModel> campains = new ArrayList<CampaignModel>();
					for (Object[] obj : campaignList) {
						CampaignModel result = new CampaignModel();

						result.setCampaignNo(obj[0].toString());
						if (obj[1] != null)
							result.setExeReason(obj[1].toString());
						if (obj[2] != null)
							result.setIssueDate(obj[2].toString());
						if (obj[3] != null)
							result.setCampaignDueDate(obj[3].toString());
						if (obj[4] != null)
							result.setCampaignType(obj[4].toString());
						if (obj[5] != null)
							result.setFileCount(Integer.parseInt(obj[5].toString()));

						campains.add(result);
					}

					reportChoiceList.setCampaignList(campains);
				}

				System.out.println("---END get campaign list at: " + LocalDateTime.now() + " | Total: "
						+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			}

			startDate = LocalDateTime.now();

			// エリアリスト
			List<Object[]> areaData = reportService.getAreaList(deviceLanguage, sumModel1);

			if (areaData != null) {
				List<AreaModel> areaList = new ArrayList<AreaModel>();
				String areaCode = "";
				AreaModel areaItem = null;

				String partsCode = "";
				List<PartsModel> partsList = null;
				PartsModel partsItem = null;

				String conditionCode = "";
				List<ConditionModel> conditionList = null;
				ConditionModel conditionItem = null;

				List<TpNoModel> tpNoList = null;

				String attachmentCode = "";
				List<AttachmentCrackModel> attachmentCrackList = null;

				int size = areaData.size();
				for (int i = 0; i < size; i++) {

					Object[] obj = areaData.get(i);
					// エリアリスト
					if (obj[0] == null)
						break;

					String areaCD = obj[0].toString();

					if (!areaCode.contentEquals(areaCD)) {

						if (areaItem != null) {
							if (tpNoList != null) {
								if (conditionItem == null)
									conditionItem = new ConditionModel();

								conditionItem.setTpNoList(tpNoList);
							}

							if (conditionItem != null) {
								if (conditionList == null)
									conditionList = new ArrayList<ConditionModel>();

								conditionList.add(conditionItem);
							}

							if (conditionList != null) {
								if (partsItem == null)
									partsItem = new PartsModel();

								partsItem.setConditionList(conditionList);
							}

							if (attachmentCrackList != null) {
								if (partsItem == null)
									partsItem = new PartsModel();

								partsItem.setAttachmentCrackList(attachmentCrackList);

							}

							if (partsItem != null) {
								if (partsList == null)
									partsList = new ArrayList<PartsModel>();

								partsList.add(partsItem);
							}

							if (partsList != null)
								areaItem.setPartsList(partsList);

							areaList.add(areaItem);
						}

						areaCode = areaCD;
						areaItem = new AreaModel();
						areaItem.setAreaCode(areaCD);
						if (obj[1] != null)
							areaItem.setAreaName(obj[1].toString());

						partsList = null;
						partsCode = "";
						partsItem = null;
						conditionList = null;
						conditionCode = "";
						conditionItem = null;
						tpNoList = null;
						attachmentCrackList = null;
						attachmentCode = "";
					}

					// パーツリスト
					if (obj[2] == null)
						break;

					String parrtsCd = obj[2].toString();

					if (!parrtsCd.contentEquals(partsCode)) {

						if (partsList == null)
							partsList = new ArrayList<PartsModel>();

						if (tpNoList != null) {
							if (conditionItem == null)
								conditionItem = new ConditionModel();

							conditionItem.setTpNoList(tpNoList);
						}

						if (conditionItem != null) {
							if (conditionList == null)
								conditionList = new ArrayList<ConditionModel>();

							conditionList.add(conditionItem);
						}

						if (conditionList != null) {
							if (partsItem == null)
								partsItem = new PartsModel();

							partsItem.setConditionList(conditionList);
						}

						if (attachmentCrackList != null) {
							if (partsItem == null)
								partsItem = new PartsModel();

							partsItem.setAttachmentCrackList(attachmentCrackList);

						}

						if (partsItem != null)
							partsList.add(partsItem);

						partsCode = parrtsCd;
						partsItem = new PartsModel();
						partsItem.setPartsCode(parrtsCd);
						if (obj[3] != null)
							partsItem.setPartsName(obj[3].toString());

						conditionList = null;
						conditionCode = "";
						tpNoList = null;
						attachmentCrackList = null;
						attachmentCode = "";

					}

					// 症状リスト
					if (obj[4] != null) {

						String conditionCd = obj[4].toString();

						if (!conditionCode.contentEquals(conditionCd)) {
							if (tpNoList != null) {
								if (conditionItem == null)
									conditionItem = new ConditionModel();

								conditionItem.setTpNoList(tpNoList);
							}
							if (conditionItem != null) {
								if (conditionList == null)
									conditionList = new ArrayList<ConditionModel>();

								conditionList.add(conditionItem);
							}

							conditionItem = new ConditionModel();
							conditionCode = conditionCd;
							tpNoList = null;

							conditionItem.setConditionCode(obj[4].toString()); // 症状コード
							if (obj[5] != null)
								conditionItem.setConditionName(obj[5].toString()); // 症状名称
						}

						if (obj[6] != null) {
							TpNoModel tpNoItem = new TpNoModel();
							tpNoItem.setTpNoId(Integer.parseInt(obj[6].toString()));
							if (obj[7] != null)
								tpNoItem.setTpNoName(obj[7].toString());

							if (tpNoList == null)
								tpNoList = new ArrayList<TpNoModel>();

							tpNoList.add(tpNoItem);
						}

					}

					if (obj[8] != null) {
						String attachmentCrackCd = obj[8].toString();
						if (!attachmentCode.contentEquals(attachmentCrackCd)) {

							AttachmentCrackModel attechmentCrackItem = new AttachmentCrackModel();
							attechmentCrackItem.setCrackCode(obj[8].toString());

							if (obj[9] != null)
								attechmentCrackItem.setCrackName(obj[9].toString());

							if (attachmentCrackList == null)
								attachmentCrackList = new ArrayList<AttachmentCrackModel>();

							attachmentCrackList.add(attechmentCrackItem);
						}
					}

				}

				// 最後データ
				if (areaItem != null) {
					if (tpNoList != null) {
						if (conditionItem == null)
							conditionItem = new ConditionModel();

						conditionItem.setTpNoList(tpNoList);
					}

					if (conditionItem != null) {
						if (conditionList == null)
							conditionList = new ArrayList<ConditionModel>();

						conditionList.add(conditionItem);
					}

					if (conditionList != null) {
						if (partsItem == null)
							partsItem = new PartsModel();

						partsItem.setConditionList(conditionList);
					}

					if (attachmentCrackList != null) {
						if (partsItem == null)
							partsItem = new PartsModel();

						partsItem.setAttachmentCrackList(attachmentCrackList);

					}

					if (partsItem != null) {
						if (partsList == null)
							partsList = new ArrayList<PartsModel>();

						partsList.add(partsItem);
					}

					if (partsList != null)
						areaItem.setPartsList(partsList);

					areaList.add(areaItem);
				}

				if (areaList.size() > 0)
					reportChoiceList.setAreaList(areaList);

				System.out.println("---END get area list at: " + LocalDateTime.now() + " | Total: "
						+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			}

			startDate = LocalDateTime.now();

			List<Object[]> warrantyData = reportService.getWarrantyList(deviceLanguage, machineId);

			if (warrantyData != null) {
				int size = warrantyData.size();
				List<WarrantyModel> warrantyList = new ArrayList<WarrantyModel>();
				for (int i = 0; i < size; i++) {
					Object[] obj = warrantyData.get(i);
					WarrantyModel item = new WarrantyModel();
					item.setWarrantyId(Double.parseDouble(obj[0].toString()));
					if (obj[1] != null)
						item.setWarrantyName(obj[1].toString());
					if (obj[2] != null)
						item.setModels(obj[2].toString());
					if (obj[3] != null)
						item.setMonthsElapsed(Double.parseDouble(obj[3].toString()));
					if (obj[4] != null)
						item.setHours(Double.parseDouble(obj[4].toString()));
					if (obj[5] != null)
						item.setRegularlyInspectionTimes(Double.parseDouble(obj[5].toString()));

					warrantyList.add(item);
				}

				if (warrantyList.size() > 0)
					reportChoiceList.setWarrantyList(warrantyList);

				System.out.println("---END get warranty list at: " + LocalDateTime.now() + " | Total: "
						+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			}
			startDate = LocalDateTime.now();

			List<Object[]> attachmentData = reportService.getCodeValueDataByCodeType(deviceLanguage, "0014");
			if (attachmentData != null) {
				int size = attachmentData.size();
				List<AttachmentModel> attachmentList = new ArrayList<AttachmentModel>();
				for (int i = 0; i < size; i++) {
					Object[] obj = attachmentData.get(i);
					AttachmentModel item = new AttachmentModel();
					item.setAttachmentCode(obj[0].toString());
					if (obj[1] != null)
						item.setAttachmentName(obj[1].toString());

					attachmentList.add(item);

				}
				if (attachmentList.size() > 0)
					reportChoiceList.setAttachmentList(attachmentList);

				System.out.println("---END get attachment list at: " + LocalDateTime.now() + " | Total: "
						+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			}
			startDate = LocalDateTime.now();
			
			List<Object[]> workSiteData = reportService.getCodeValueDataByCodeType(deviceLanguage, "0013");
			if (workSiteData != null) {
				int size = workSiteData.size();
				List<WorkSiteModel> workSiteList = new ArrayList<WorkSiteModel>();
				for (int i = 0; i < size; i++) {
					Object[] obj = workSiteData.get(i);
					WorkSiteModel item = new WorkSiteModel();
					item.setWorkSiteCode(obj[0].toString());
					if (obj[1] != null)
						item.setWorkSiteName(obj[1].toString());

					workSiteList.add(item);

				}
				if (workSiteList.size() > 0)
					reportChoiceList.setWorkSiteList(workSiteList);

				System.out.println("---END get workSite list at: " + LocalDateTime.now() + " | Total: "
						+ Duration.between(startDate, LocalDateTime.now()).toMillis());
			}
			
			System.out.println("---END Call reportChoiceList API at: " + LocalDateTime.now() + " | Total: "
					+ Duration.between(startDateRoot, LocalDateTime.now()).toMillis());
			
			

			reportChoiceList.setStatusCode(Constants.CON_OK);
			return Response.ok(reportChoiceList).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}
}