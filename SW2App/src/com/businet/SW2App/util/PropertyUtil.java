package com.businet.SW2App.util;



import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class PropertyUtil {


	private PropertyUtil() {}


	/**
	 * 外部ファイル（properties）から key,value　読み込み
	 */
	public static String readProperties(String filePath, String key, String defaultValue) throws Exception{

		Properties properties = new Properties();

		try {

			InputStream inputStream = new FileInputStream(filePath);
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
			BufferedReader reader = new BufferedReader(inputStreamReader);
			properties.load(reader);

			inputStream.close();
			inputStreamReader.close();

			if(properties.getProperty(key)!=null)
				return properties.getProperty(key);
			else
				return defaultValue;

		}catch(Exception e) {
			throw new Exception("readPropertys error.");
		}

	}

}