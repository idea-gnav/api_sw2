package com.businet.SW2App.models;

import java.util.List;

public class CountryModel {

	String countryCode;
	String countryName;
	List<StateModel> stateList;
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public List<StateModel> getStateList() {
		return stateList;
	}
	public void setStateList(List<StateModel> stateList) {
		this.stateList = stateList;
	}

	
}
