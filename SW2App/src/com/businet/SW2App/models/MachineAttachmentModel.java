package com.businet.SW2App.models;

public class MachineAttachmentModel {

	String machineAttachmentCode;
	String machineAttachmentName;
	
	public String getMachineAttachmentCode() {
		return machineAttachmentCode;
	}
	public void setMachineAttachmentCode(String machineAttachmentCode) {
		this.machineAttachmentCode = machineAttachmentCode;
	}
	public String getMachineAttachmentName() {
		return machineAttachmentName;
	}
	public void setMachineAttachmentName(String machineAttachmentName) {
		this.machineAttachmentName = machineAttachmentName;
	}
	
}
