package com.businet.SW2App.ejbs.report;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.SW2App.ConvertUtil;

@Stateless
@Local(IReportService.class)
public class ReportService implements IReportService {

	@PersistenceContext(unitName = "SW2App")
	EntityManager em;

	@Override
	public String getProcessDefinCodeById(Integer id) {
		em.getEntityManagerFactory().getCache().evictAll();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("process_defin_code ");
		sb.append("FROM mst_process_defin ");
		sb.append("WHERE id = ?1");

		@SuppressWarnings("unchecked")
		List<Object> result = em.createNativeQuery(sb.toString()).setParameter(1, id).getResultList();

		if (result.size() > 0)
			return result.get(0).toString();

		return null;

	}

	// �����X�g
	@Override
	public List<Object[]> getCountryListByLanguage(Integer deviceLanguage) {
		em.getEntityManagerFactory().getCache().evictAll();

		String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("v1.code,v1.code_value_1, ");
		sb.append("v2.code,v2.code_value_1,  ");
		sb.append("v3.code,v3.code_value_1  ");
		sb.append("FROM mst_code_value_v v1 ");
		sb.append("LEFT JOIN mst_code_value_v v2 ");
		sb.append("on v1.code = v2.code_value_2 ");
		sb.append("and v2.code_type='0002' ");
		sb.append("and v2.language =?1 ");
		sb.append("LEFT JOIN mst_code_value_v v3 ");
		sb.append("on v3.code_value_3 = v2.code ");
		sb.append("and v3.code_value_2 = v1.code ");
		sb.append("and v3.code_type='0068' ");
		sb.append("and v3.language = ?1 ");
		sb.append("WHERE v1.code_type='0001' and v1.language = ?1 ");
		sb.append("order by v1.code ,v2.code ,v3.code ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(sb.toString()).setParameter(1, languageCd).getResultList();

		if (result.size() > 0)
			return result;

		return null;

	}

	@Override
	public List<Object[]> getCodeValueDataByCodeType(Integer deviceLanguage, String codeType) {
		em.getEntityManagerFactory().getCache().evictAll();
		String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("code,code_value_1 ");
		sb.append("FROM mst_code_value_v ");
		sb.append("WHERE language = ?1 and code_type= ?2 ");
		sb.append("order by code ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(sb.toString()).setParameter(1, languageCd)
				.setParameter(2, codeType).getResultList();

		if (result.size() > 0)
			return result;

		return null;

	}

	@Override
	public List<Object[]> getCheckTypeListByMachineNumber(String machineNumber) {

		em.getEntityManagerFactory().getCache().evictAll();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("mrit.id, ");
		sb.append("mrit.inspection_name ");
		sb.append("FROM ");
		sb.append("mst_machine mm ");
		sb.append("JOIN mst_warranty_type mwt ON mm.warranty_type_id = mwt.id ");
		sb.append("JOIN mst_regularly_inspection_type mrit  ");
		sb.append("ON mwt.warranty_type_code = mrit.security_type_code ");
		sb.append("WHERE ");
		sb.append("mm.machine_number ='" + machineNumber + "' ");
		sb.append("ORDER BY mrit.id ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(sb.toString().replaceAll("\"", "")).getResultList();

		if (result.size() > 0)
			return result;

		return null;

	}

	@Override
	public List<Object[]> getCampaignListByMachineNumber(Integer deviceLanguage, String machineNumber) {

		em.getEntityManagerFactory().getCache().evictAll();

		String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("mc.campaign_no, ");
		sb.append("mc.exe_reason, ");
		sb.append("mc.issue_date, ");
		sb.append("mc.campaign_due_date, ");
		sb.append("mcvv.code_value_1, ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("COUNT(*) ");
		sb.append("FROM ");
		sb.append("tbl_attachment_file taf ");
		sb.append("WHERE ");
		sb.append("taf.process_id = mc.process_id ");
		sb.append("AND   report_type = '130' ");
		sb.append(") AS attachmentcount ");
		sb.append("FROM ");
		sb.append("mst_campaign_machine mcm ");
		sb.append("LEFT JOIN mst_campaign mc ON mcm.campaign_id = mc.id ");
		sb.append("LEFT JOIN mst_code_value_v mcvv ON ( mc.improvement_work_section = mcvv.code ");
		sb.append("AND mcvv.code_type = '0061' ");
		sb.append("AND mcvv.language = '" + languageCd + "' ) ");
		sb.append("WHERE ");
		sb.append("machine_number = '" + machineNumber + "' ");
		sb.append("AND   mc.effective_disabled = 1 ");
		sb.append("AND   mc.release = 1 ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(sb.toString().replaceAll("\"", "")).getResultList();

		if (result.size() > 0)
			return result;

		return null;

	}

	@Override
	public List<Object[]> getAreaList(Integer deviceLanguage, String models) {

		em.getEntityManagerFactory().getCache().evictAll();

		String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("v1.code,v1.code || v1.code_value_1 as name1, ");// areaList
		sb.append("v2.code, v2.code || v2.code_value_1 as name2, ");// partsList
		sb.append("v4.code,v4.code || v4.code_value_1 as name4, ");// conditionList
		sb.append("mlc.id,mlc.tpno, "); // tpNoList
		sb.append("v3.code,v3.code || v3.code_value_1 as name3 "); // attachmentCrackList
		sb.append("FROM ");
		sb.append("mst_code_value_v v1 ");
		sb.append("LEFT JOIN mst_code_value_v v2 ");
		sb.append("on substr(v2.code,1,1) = v1.code ");
		sb.append("and v2.code_type = '0064' ");
		sb.append("and v2.language = ?1 ");
		sb.append("LEFT JOIN mst_code_value_v v3 ");
		sb.append("on v3.CODE_VALUE_2 = v2.code ");
		sb.append("and v3.code_type = '0054' ");
		sb.append("and v3.language = ?1  ");
		sb.append("LEFT JOIN mst_code_value_v v4 ");
		sb.append("on v4.code_type = '0066' ");
		sb.append("and v4.language = ?1 ");
		sb.append("LEFT JOIN mst_labor_code mlc ");
		sb.append("on mlc.area_code = v1.code ");
		sb.append("and mlc.parts_code = v2.code ");
		sb.append("and mlc.condition_code = v4.code ");
		sb.append("and mlc.models = ?2 ");
		sb.append("and mlc.code_type = 2 ");
		sb.append("WHERE ");
		sb.append("v1.code_type = '0064' ");
		sb.append("AND v1.language = ?1 ");
		sb.append("ORDER BY ");
		sb.append("v1.code,v2.code,v4.code,v3.code,mlc.tpno ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(sb.toString()).setParameter(1, languageCd).setParameter(2, models)
				.getResultList();

		if (result.size() > 0)
			return result;

		return null;

	}

	@Override
	public Object[] getMachineInfoById(Integer machineId) {

		em.getEntityManagerFactory().getCache().evictAll();

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("id, ");
		sb.append("machine_number, ");
		sb.append("summary_model_1 ");
		sb.append("FROM ");
		sb.append("mst_machine ");
		sb.append("WHERE ");
		sb.append("id = ?1 ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(sb.toString()).setParameter(1, machineId).getResultList();

		if (result.size() > 0)
			return result.get(0);

		return null;
	}

	@Override
	public List<Object[]> getWarrantyList(Integer deviceLanguage, Integer machineId) {

		em.getEntityManagerFactory().getCache().evictAll();
		String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("mwt.id, ");
		sb.append("mcvv.code_value_1, ");
		sb.append("mwt.MODELS, ");
		sb.append("mwt.months_elapsed, ");
		sb.append("mwt.hours,   ");
		sb.append("mwt.regularly_inspection_times ");
		sb.append("FROM ");
		sb.append("mst_machine mm ");
		sb.append("left join mst_org_system_setting moss on mm.org_id = moss.org_id ");
		sb.append("left join mst_warranty_type mwt on moss.MST_WARRANTY_PATTERN = mwt.PATTERN ");
		sb.append("LEFT JOIN mst_code_value_v mcvv ON ( mwt.warranty_type_code = mcvv.code ");
		sb.append("AND mcvv.CODE_TYPE = '0033' ");
		sb.append("AND mcvv.language = ?1) ");
		sb.append("where  ");
		sb.append("mm.id = ?2");
		sb.append("and mwt.MODELS = mm.SUMMARY_MODEL_1 ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(sb.toString()).setParameter(1, languageCd)
				.setParameter(2, machineId).getResultList();

		if (result.size() > 0)
			return result;
		else {
			sb = new StringBuilder();
			sb.append("SELECT ");
			sb.append("mwt.id, ");
			sb.append("mcvv.code_value_1, ");
			sb.append("mwt.MODELS, ");
			sb.append("mwt.months_elapsed, ");
			sb.append("mwt.hours,   ");
			sb.append("mwt.regularly_inspection_times ");
			sb.append("FROM ");
			sb.append("mst_machine mm ");
			sb.append("left join mst_org_system_setting moss on mm.org_id = moss.org_id ");
			sb.append("left join mst_warranty_type mwt on moss.MST_WARRANTY_PATTERN = mwt.PATTERN ");
			sb.append("LEFT JOIN mst_code_value_v mcvv ON ( mwt.warranty_type_code = mcvv.code ");
			sb.append("AND mcvv.CODE_TYPE = '0033' ");
			sb.append("AND mcvv.language = ?1) ");
			sb.append("where  ");
			sb.append("mm.id = ?2");
			sb.append("and mwt.MODELS IS NUll ");

			@SuppressWarnings("unchecked")
			List<Object[]> result2 = em.createNativeQuery(sb.toString()).setParameter(1, languageCd)
					.setParameter(2, machineId).getResultList();

			if (result2.size() > 0)
				return result2;
		}

		return null;
	}

}
