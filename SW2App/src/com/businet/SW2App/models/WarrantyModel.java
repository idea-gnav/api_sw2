package com.businet.SW2App.models;

public class WarrantyModel {
	double warrantyId;
	String warrantyName;
	String models;
	double monthsElapsed;
	double hours;
	double regularlyInspectionTimes;
	
	public double getWarrantyId() {
		return warrantyId;
	}
	public void setWarrantyId(double warrantyId) {
		this.warrantyId = warrantyId;
	}
	public String getWarrantyName() {
		return warrantyName;
	}
	public void setWarrantyName(String warrantyName) {
		this.warrantyName = warrantyName;
	}
	public String getModels() {
		return models;
	}
	public void setModels(String models) {
		this.models = models;
	}
	public double getMonthsElapsed() {
		return monthsElapsed;
	}
	public void setMonthsElapsed(double monthsElapsed) {
		this.monthsElapsed = monthsElapsed;
	}
	public double getHours() {
		return hours;
	}
	public void setHours(double hours) {
		this.hours = hours;
	}
	public double getRegularlyInspectionTimes() {
		return regularlyInspectionTimes;
	}
	public void setRegularlyInspectionTimes(double regularlyInspectionTimes) {
		this.regularlyInspectionTimes = regularlyInspectionTimes;
	}
	
	

}
