package com.businet.SW2App.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;




public class Base64 {

	private static final Logger logger = Logger.getLogger(PropertyUtil.class.getName());

	private Base64() { }



	public static byte[] decodeBase64(String data) {
	    byte[] urlsafe = data.getBytes();
	    int mod = urlsafe.length % 4;
	    byte[] nosafe = new byte[urlsafe.length + mod];
	    for (int i = 0; i < urlsafe.length; i++) {
	        if (urlsafe[i] == 0x2d) {
	            nosafe[i] = 0x2b;
	        }
	        else if (urlsafe[i] == 0x5f) {
	            nosafe[i] = 0x2f;
	        }
	        else {
	            nosafe[i] = urlsafe[i];
	        }
	    }
	    
	    for (int i = urlsafe.length + mod - 1; i >= urlsafe.length; i--) {
	        nosafe[i] = 0x3d;
	    }

	    ByteArrayInputStream from = new ByteArrayInputStream(nosafe);
	    try (InputStream is = MimeUtility.decode(from, "base64"); ByteArrayOutputStream to = new ByteArrayOutputStream()) {
	        byte[] buf = new byte[8192];
	        int readCnt;
	        while ((readCnt = is.read(buf)) != -1) {
	            to.write(buf, 0, readCnt);
	        }
	        to.flush();
	        return to.toByteArray();

	    } catch (MessagingException | IOException e) {
	        e.printStackTrace();
	        return "Bad Decryption".getBytes();
	    }
	}


	public static String encodeBase64(byte[] data) {
	    try (ByteArrayOutputStream encodedChunked = new ByteArrayOutputStream()) {
	        try (OutputStream os = MimeUtility.encode(encodedChunked, "base64")) {
	            os.write(data);
	            os.flush();
	        }
	       
	        String encodedStr = trimCRLF(encodedChunked);
	        return encodedStr;

	    } catch (IOException e) {
	        e.printStackTrace();
	        return "Bad Encryption";
	    } catch (MessagingException e) {
	        e.printStackTrace();
	        return "Bad Encryption";
	    }
	}

	private static String trimCRLF(ByteArrayOutputStream encodedCRLF) {
	    byte inputArray[] = encodedCRLF.toByteArray();
	    byte outputArray[] = new byte[encodedCRLF.size()];

	    int n = 0;
	    for (int i = 0; i < encodedCRLF.size() - 1; i++) {
	        if (inputArray[i] == 0x0d) {// CR
	            if (inputArray[i + 1] == 0x0a) {// LF
	                i++;
	                continue;
	            }
	        }
	        outputArray[n] = inputArray[i];
	        n++;
	    }
	    return new String(outputArray, 0, n);
	}




}
