package com.businet.SW2App.models;

public class TpNoModel {
	int tpNoId;
	String tpNoName;
	
	public int getTpNoId() {
		return tpNoId;
	}
	public void setTpNoId(int tpNoId) {
		this.tpNoId = tpNoId;
	}
	public String getTpNoName() {
		return tpNoName;
	}
	public void setTpNoName(String tpNoName) {
		this.tpNoName = tpNoName;
	}

}
