package com.businet.SW2App.models;

public class MachineStatusModel {

	String machineStatusCode;
	String machineStatusName;
	
	public String getMachineStatusCode() {
		return machineStatusCode;
	}
	public void setMachineStatusCode(String machineStatusCode) {
		this.machineStatusCode = machineStatusCode;
	}
	public String getMachineStatusName() {
		return machineStatusName;
	}
	public void setMachineStatusName(String machineStatusName) {
		this.machineStatusName = machineStatusName;
	}

}
