package com.businet.SW2App.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.businet.SW2App.Constants;
import com.businet.SW2App.annotations.Authorized;
import com.businet.SW2App.ejbs.report.IReportService;
import com.businet.SW2App.http.HttpServices;
import com.businet.SW2App.http.HttpServices.RequestType;
import com.businet.SW2App.models.ReportMenuListModel;
import com.businet.SW2App.models.ReturnContainer;


import oracle.jdbc.Const;

@Path("/reportMenuList")
@Stateless
public class ReportMenuListResource {

	private static final Logger logger = Logger.getLogger(ReportMenuListResource.class.getName());

	@EJB
	IReportService reportService;

	/**
	 * [API No.8]  レポートメニュー一覧
	 * 
	 * @param sw2UserId
	 * @param deviceLanguage
	 * @param machineId
	 * @param machineNumber
	 * @param sw2Token
	 * @return Response.JSON ReportMenuListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("sw2UserId") String sw2UserId,
			@FormParam("deviceLanguage") Integer deviceLanguage, @FormParam("machineId") Integer machineId,
			@FormParam("machineNumber") String machineNumber, @FormParam("sw2Token") String sw2Token) {

		logger.info("[GNAV][POST] sw2UserId=" + sw2UserId + ", deviceLanguage=" + deviceLanguage + ", machineId="
				+ machineId + ", machineNumber=" + machineNumber + ", sw2Token=" + sw2Token);

		try {

			String targetUrl = Constants.SW2_HOST_URL + "/t/t02p01/init_data/";

			ReportMenuListModel reportMenu = new ReportMenuListModel();

			String params = "ID=" + machineId + "&MACHINE_NUMBER=" + machineNumber + "&title=文書選択画面";
			String rs = HttpServices.sendHttpRequest(targetUrl, RequestType.POST, params, sw2Token);

			if (rs != null) {
				
				JSONObject jObj = new JSONObject(rs);

				if (jObj != null) {
					if (jObj.has("data")) {
						JSONObject data = (JSONObject) jObj.get("data");

						if (data.has("buttonInfo")) {

							JSONArray buttonList = (JSONArray) data.get("buttonInfo");

							for (Object object : buttonList) {
								if (object instanceof JSONObject) {
									JSONObject btn = (JSONObject) object;
									
									int _id = Integer.parseInt((String) btn.get("ID"));
									String report_type = (String) btn.get("REPORT_TYPE");
									String process_defin_code = reportService.getProcessDefinCodeById(_id);
									
									switch (report_type) {
									case Constants.REPORT_TYPE_010:
										reportMenu.setReportFlg010(1);
										reportMenu.setDefCd010(process_defin_code);
										break;
									case Constants.REPORT_TYPE_020:
										reportMenu.setReportFlg020(1);
										reportMenu.setDefCd020(process_defin_code);
										break;
									case Constants.REPORT_TYPE_030:
										reportMenu.setReportFlg030(1);
										reportMenu.setDefCd030(process_defin_code);
										break;
									case Constants.REPORT_TYPE_040:
										reportMenu.setReportFlg040(1);
										reportMenu.setDefCd040(process_defin_code);
										break;
									case Constants.REPORT_TYPE_050:
										reportMenu.setReportFlg050(1);
										reportMenu.setDefCd050(process_defin_code);
										break;
									case Constants.REPORT_TYPE_055:
										reportMenu.setReportFlg055(1);
										reportMenu.setDefCd055(process_defin_code);
										break;
									case Constants.REPORT_TYPE_060:
										reportMenu.setReportFlg060(1);
										reportMenu.setDefCd060(process_defin_code);
										break;
									case Constants.REPORT_TYPE_070:
										reportMenu.setReportFlg070(1);
										reportMenu.setDefCd070(process_defin_code);
										break;
									case Constants.REPORT_TYPE_080:
										reportMenu.setReportFlg080(1);
										reportMenu.setDefCd080(process_defin_code);
										break;
									case Constants.REPORT_TYPE_090:
										reportMenu.setReportFlg090(1);
										reportMenu.setDefCd090(process_defin_code);
										break;

									}
								}
							}
						}
					}
				}

				reportMenu.setStatusCode(Constants.CON_OK);
				return Response.ok(reportMenu).build();

			} else
				return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}
}