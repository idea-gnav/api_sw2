package com.businet.SW2App.models;


public class ReportMenuListModel extends BaseModel {

	int reportFlg010;
	String defCd010;
	int reportFlg020;
	String defCd020;
	int reportFlg030;
	String defCd030;
	int reportFlg040;
	String defCd040;
	int reportFlg050;
	String defCd050;
	int reportFlg055;
	String defCd055;
	int reportFlg060;
	String defCd060;
	int reportFlg070;
	String defCd070;
	int reportFlg080;
	String defCd080;
	int reportFlg090;
	String defCd090;
	int surveyFlg;
	
	
	public int getReportFlg010() {
		return reportFlg010;
	}
	public void setReportFlg010(int reportFlg010) {
		this.reportFlg010 = reportFlg010;
	}
	public String getDefCd010() {
		return defCd010;
	}
	public void setDefCd010(String defCd010) {
		this.defCd010 = defCd010;
	}
	public int getReportFlg020() {
		return reportFlg020;
	}
	public void setReportFlg020(int reportFlg020) {
		this.reportFlg020 = reportFlg020;
	}
	public String getDefCd020() {
		return defCd020;
	}
	public void setDefCd020(String defCd020) {
		this.defCd020 = defCd020;
	}
	public int getReportFlg030() {
		return reportFlg030;
	}
	public void setReportFlg030(int reportFlg030) {
		this.reportFlg030 = reportFlg030;
	}
	public String getDefCd030() {
		return defCd030;
	}
	public void setDefCd030(String defCd030) {
		this.defCd030 = defCd030;
	}
	public int getReportFlg040() {
		return reportFlg040;
	}
	public void setReportFlg040(int reportFlg040) {
		this.reportFlg040 = reportFlg040;
	}
	public String getDefCd040() {
		return defCd040;
	}
	public void setDefCd040(String defCd040) {
		this.defCd040 = defCd040;
	}
	public int getReportFlg050() {
		return reportFlg050;
	}
	public void setReportFlg050(int reportFlg050) {
		this.reportFlg050 = reportFlg050;
	}
	public String getDefCd050() {
		return defCd050;
	}
	public void setDefCd050(String defCd050) {
		this.defCd050 = defCd050;
	}
	public int getReportFlg055() {
		return reportFlg055;
	}
	public void setReportFlg055(int reportFlg055) {
		this.reportFlg055 = reportFlg055;
	}
	public String getDefCd055() {
		return defCd055;
	}
	public void setDefCd055(String defCd055) {
		this.defCd055 = defCd055;
	}
	
	public int getReportFlg060() {
		return reportFlg060;
	}
	public void setReportFlg060(int reportFlg060) {
		this.reportFlg060 = reportFlg060;
	}
	public String getDefCd060() {
		return defCd060;
	}
	public void setDefCd060(String defCd060) {
		this.defCd060 = defCd060;
	}
	
	public int getReportFlg070() {
		return reportFlg070;
	}
	public void setReportFlg070(int reportFlg070) {
		this.reportFlg070 = reportFlg070;
	}
	public String getDefCd070() {
		return defCd070;
	}
	public void setDefCd070(String defCd070) {
		this.defCd070 = defCd070;
	}
	public int getReportFlg080() {
		return reportFlg080;
	}
	public void setReportFlg080(int reportFlg080) {
		this.reportFlg080 = reportFlg080;
	}
	public String getDefCd080() {
		return defCd080;
	}
	public void setDefCd080(String defCd080) {
		this.defCd080 = defCd080;
	}
	public int getReportFlg090() {
		return reportFlg090;
	}
	public void setReportFlg090(int reportFlg090) {
		this.reportFlg090 = reportFlg090;
	}
	public String getDefCd090() {
		return defCd090;
	}
	public void setDefCd090(String defCd090) {
		this.defCd090 = defCd090;
	}
	public int getSurveyFlg() {
		return surveyFlg;
	}
	public void setSurveyFlg(int surveyFlg) {
		this.surveyFlg = surveyFlg;
	}
	

}
